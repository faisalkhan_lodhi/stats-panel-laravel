<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

            // Admin

Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function() {

    Route::resource('roles','RoleController');
    Route::resource('users','UserController');

});

           //Telenor

Route::group(['prefix' => 'telenor' , 'middleware' => ['role:admin|Telenor']], function() {


    Route::get('/stats/{sid}/{level}/{it}', 'TelenorController@stats');

//    Route::get('/bizzstore/daily', 'TelenorController@daily');
//    Route::get('/bizzstore/monthly', 'TelenorController@monthly');
//    Route::get('/foodcourt/daily', 'TelenorController@foodDaily');
//    Route::get('/foodcourt/monthly', 'TelenorController@foodMonthly');

});

          //Mobilink

Route::group(['prefix' => 'mobilink' , 'middleware' => ['role:admin|Mobilink']], function() {

    Route::get('/stats/{sid}/{level}/{it}', 'MobilinkController@stats');

//    Route::get('/bizzstore/daily', 'MobilinkController@bizzDaily');
//    Route::get('/bizzstore/monthly', 'MobilinkController@bizzMonthly');
//    Route::get('/cc/daily', 'MobilinkController@ccDaily');
//    Route::get('/cc/monthly', 'MobilinkController@ccMonthly');
//    Route::get('/islam/daily', 'MobilinkController@islamDaily');
//    Route::get('/islam/monthly', 'MobilinkController@islamMonthly');
//    Route::get('/kc/daily', 'MobilinkController@kcDaily');
//    Route::get('/kc/monthly', 'MobilinkController@kcMonthly');
//    Route::get('/rozgar/daily', 'MobilinkController@rozgarDaily');
//    Route::get('/rozgar/monthly', 'MobilinkController@rozgarMonthly');
//    Route::get('/sciencelab/daily', 'MobilinkController@sciencelabDaily');
//    Route::get('/sciencelab/monthly', 'MobilinkController@sciencelabMonthly');

});


         //Ufone

Route::group(['prefix' => 'ufone' , 'middleware' => ['role:admin|Ufone']], function() {


    Route::get('/stats/{sid}/{level}/{it}', 'UfoneController@stats');


//    Route::get('/bizzstore/daily', 'UfoneController@bizzDaily');
//    Route::get('/bizzstore/monthly', 'UfoneController@bizzMonthly');
//    Route::get('/foodcourt/daily', 'UfoneController@foodcourtDaily');
//    Route::get('/foodcourt/monthly', 'UfoneController@foodcourtMonthly');
//    Route::get('/rbt/daily', 'UfoneController@rbtDaily');
//    Route::get('/rbt/monthly', 'UfoneController@rbtMonthly');
//    Route::get('/uradio/daily', 'UfoneController@uradioDaily');
//    Route::get('/uradio/monthly', 'UfoneController@uradioMonthly');

});

        // FOR WARID

Route::group(['prefix' => 'warid' , 'middleware' => ['role:admin|Warid']], function() {


    Route::get('/stats/{sid}/{level}/{it}', 'WaridController@stats');

//    Route::get('/bizzstore/daily', 'WaridController@bizzDaily');
//    Route::get('/bizzstore/monthly', 'WaridController@bizzMonthly');
//    Route::get('/cp/daily', 'WaridController@cpDaily');
//    Route::get('/cp/monthly', 'WaridController@cpMonthly');

});

        //FOR ZONG

Route::group(['prefix' => 'zong' , 'middleware' => ['role:admin|Zong']], function() {

    Route::get('/stats/{sid}/{level}/{it}', 'ZongController@stats');

//    Route::get('/bizzstore/daily', 'ZongController@bizzDaily');
//    Route::get('/bizzstore/monthly', 'ZongController@bizzMonthly');
//    Route::get('/cp/daily', 'ZongController@cpDaily');
//    Route::get('/cp/monthly', 'ZongController@cpMonthly');
//    Route::get('/kc/daily', 'ZongController@kcDaily');
//    Route::get('/kc/monthly', 'ZongController@kcMonthly');
//    Route::get('/langtutor/daily', 'ZongController@langtutorDaily');
//    Route::get('/langtutor/monthly', 'ZongController@langtutorMonthly');
//    Route::get('/rbt/daily', 'ZongController@rbtDaily');
//    Route::get('/rbt/monthly', 'ZongController@rbtMonthly');
//    Route::get('/rozgar/daily', 'ZongController@rozgarDaily');
//    Route::get('/rozgar/monthly', 'ZongController@rozgarMonthly');
//    Route::get('/sciencelab/daily', 'ZongController@sciencelabDaily');
//    Route::get('/sciencelab/monthly', 'ZongController@sciencelabMonthly');

});


