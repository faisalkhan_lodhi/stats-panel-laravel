<?php

namespace App\Http\Controllers;

use App\Models\Definition;
use Illuminate\Http\Request;

use App\Models\ufone_bizstore_daily;
use App\Models\ufone_bizstore_monthly;

use App\Models\ufone_foodcourt_daily;
use App\Models\ufone_foodcourt_monthly;

use App\Models\ufone_rbt_daily;
use App\Models\ufone_rbt_monthly;

use App\Models\ufone_uradio_daily;
use App\Models\ufone_uradio_monthly;

use DB;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\FilterConfig;
use App\Models\Shared;


class UfoneController extends Controller
{
    //
    public function stats($sid, $level, $it)
    {
        $op = Definition::UFONE_OP;
        $col = Shared::getColumns($op, $sid, $level, $it);

        $tblName = Definition::getCurrentModel($op, $sid, $level, $it);

        $query = DB::getDoctrineConnection()->createQueryBuilder();
        $query
            ->select([
                '*',
                //'platformbase',
                // 'minutes'
            ])
            ->from($tblName);
        $cfg = (new GridConfig())
            ->setDataProvider(new DbalDataProvider($query))
            ->setPageSize(8)
            ->setColumns($col);
        $grid = new Grid($cfg);
        /* $grid = Grids::make($cfg);*/
        return view('ufone.stats', compact('grid'));

    }
//    public function bizzDaily()
//    {
//        $bizzstoreDaily = ufone_bizstore_daily::orderBy('date' , 'DESC')->paginate(8);
//        return view('ufone.bizzstore.daily', compact('bizzstoreDaily'));
//    }
//
//    public function bizzMonthly()
//    {
//        $bizzstoreMonthly = ufone_bizstore_monthly::orderBy('date' , 'DESC')->paginate(8);
//        return view('ufone.bizzstore.monthly', compact('bizzstoreMonthly'));
//    }
//
//    public function foodcourtDaily()
//    {
//        $foodcourtDaily = ufone_foodcourt_daily::orderBy('date', 'DESC')->paginate(8);
//        return view('ufone.foodcourt.daily', compact('foodcourtDaily'));
//    }
//
//    public function foodcourtMonthly()
//    {
//        $foodcourtMonthly = ufone_foodcourt_monthly::orderBy('date', 'DESC')->paginate(8);
//        return view('ufone.foodcourt.monthly', compact('foodcourtMonthly'));
//    }
//
//    public function rbtDaily()
//    {
//        $rbtDaily = ufone_rbt_daily::orderBy('date', 'DESC')->paginate(8);
//        return view('ufone.rbt.daily', compact('rbtDaily'));
//    }
//
//    public function rbtMonthly()
//    {
//        $rbtMonthly = ufone_rbt_monthly::orderBy('date', 'DESC')->paginate(8);
//        return view('ufone.rbt.monthly', compact('rbtMonthly'));
//    }
//
//    public function uradioDaily()
//    {
//        $uradioDaily = ufone_uradio_daily::orderBy('date', 'DESC')->paginate(8);
//        return view('ufone.uradio.daily', compact('uradioDaily'));
//    }
//
//    public function uradioMonthly()
//    {
//        $uradioMonthly = ufone_uradio_monthly::orderBy('date', 'DESC')->paginate(8);
//        return view('ufone.uradio.monthly', compact('uradioMonthly'));
//    }

}
