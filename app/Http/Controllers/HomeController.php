<?php

namespace App\Http\Controllers;
use DB;
use App\Models\TelenorSmsCounter;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $telenor_sms_counters = DB::table('telenor_sms_counter')->simplePaginate(15);
        return view('home', compact('telenor_sms_counters'));
    }
}
