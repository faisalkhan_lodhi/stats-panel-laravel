<?php

namespace App\Http\Controllers;

use App\Models\Definition;
use Illuminate\Http\Request;
use App\Models\mobilink_bizstore_daily;
use App\Models\mobilink_bizstore_monthly;

use App\Models\mobilink_cc_daily;
use App\Models\mobilink_cc_monthly;

use App\Models\mobilink_islam_daily;
use App\Models\mobilink_islam_monthly;

use App\Models\mobilink_kc_daily;
use App\Models\mobilink_kc_monthly;

use App\Models\mobilink_rozgar_daily;
use App\Models\mobilink_rozgar_monthly;

use App\Models\mobilink_sciencelab_daily;
use App\Models\mobilink_sciencelab_monthly;
use DB;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\FilterConfig;
use App\Models\Shared;


class MobilinkController extends Controller
{
    //

    public function stats($sid, $level, $it)
    {
        $op = Definition::MOBILINK_OP;
        $col = Shared::getColumns($op, $sid, $level, $it);

        $tblName = Definition::getCurrentModel($op, $sid, $level, $it);

        $query = DB::getDoctrineConnection()->createQueryBuilder();
        $query
            ->select([
                '*',
                //'platformbase',
                // 'minutes'
            ])
            ->from($tblName);
        $cfg = (new GridConfig())
            ->setDataProvider(new DbalDataProvider($query))
            ->setPageSize(8)
            ->setColumns($col);
        $grid = new Grid($cfg);
        /* $grid = Grids::make($cfg);*/
        return view('mobilink.stats', compact('grid'));

    }

//    public function bizzDaily()
//    {
//        $bizz = mobilink_bizstore_daily::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.bizzstore.daily', compact('bizz'));
//    }
//    public function bizzMonthly()
//    {
//        $bizzMonthly = mobilink_bizstore_monthly::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.bizzstore.monthly', compact('bizzMonthly'));
//    }
//
//    public function ccDaily()
//    {
//        $CC = mobilink_cc_daily::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.cc.daily', compact('CC'));
//    }
//
//    public function ccMonthly()
//    {
//        $CCMonthly = mobilink_cc_monthly::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.cc.monthly', compact('CCMonthly'));
//    }
//
//    public function islamDaily()
//    {
//        $islamDaily = mobilink_islam_daily::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.islam.daily', compact('islamDaily'));
//    }
//
//    public function islamMonthly()
//    {
//        $islamMonthly = mobilink_islam_monthly::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.islam.monthly', compact('islamMonthly'));
//    }
//
//    public function kcDaily()
//    {
//        $kcDaily = mobilink_kc_daily::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.kc.daily', compact('kcDaily'));
//    }
//
//    public function kcMonthly()
//    {
//        $kcMonthly = mobilink_kc_monthly::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.kc.monthly', compact('kcMonthly'));
//    }
//
//    public function rozgarDaily()
//    {
//        $rozgarDaily = mobilink_rozgar_daily::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.rozgar.daily', compact('rozgarDaily'));
//    }
//
//    public function rozgarMonthly()
//    {
//        $rozgarMonthly = mobilink_rozgar_monthly::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.rozgar.monthly', compact('rozgarMonthly'));
//    }
//
//    public function sciencelabDaily()
//    {
//        $sciencelabDaily = mobilink_sciencelab_daily::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.sciencelab.daily', compact('sciencelabDaily'));
//    }
//
//    public function sciencelabMonthly()
//    {
//        $sciencelabMonthly = mobilink_sciencelab_monthly::orderBy('date', 'DESC')->paginate(8);
//        return view('mobilink.sciencelab.monthly', compact('sciencelabMonthly'));
//    }

}
