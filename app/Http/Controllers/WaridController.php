<?php

namespace App\Http\Controllers;
use App\Models\Definition;
use App\User;
use App\WaridBizstoreDaily;
use DB;
use Illuminate\Http\Request;

use App\Models\warid_bizstore_daily;
use App\Models\warid_bizstore_monthly;

use App\Models\warid_cp_daily;
use App\Models\warid_cp_monthly;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\FilterConfig;
use App\Models\Shared;


class WaridController extends Controller
{
    //

    public function stats($sid, $level, $it)
    {
        $op = Definition::WARID_OP;
        $col = Shared::getColumns($op, $sid, $level, $it);

        $tblName = Definition::getCurrentModel($op, $sid, $level, $it);
        $query = DB::getDoctrineConnection()->createQueryBuilder();
        $query
            ->select([
                '*',
            ])
            ->from($tblName);
        $cfg = (new GridConfig())
            ->setDataProvider(new DbalDataProvider($query))
            ->setPageSize(8)
            ->setColumns($col);

        $grid = new Grid($cfg);
        /* $grid = Grids::make($cfg);*/
        return view('warid.stats', compact('grid'));
    }
}
