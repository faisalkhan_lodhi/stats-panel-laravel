<?php

namespace App\Http\Controllers;
use App\Models\Definition;
use DB;
use Illuminate\Http\Request;

use App\Models\zong_bizstore_daily;
use App\Models\zong_bizstore_monthly;

use App\Models\zong_cp_daily;
use App\Models\zong_cp_monthly;

use App\Models\zong_kc_daily;
use App\Models\zong_kc_monthly;

use App\Models\zong_langtutor_daily;
use App\Models\zong_langtutor_monthly;

use App\Models\zong_rbt_daily;
use App\Models\zong_rbt_monthly;

use App\Models\zong_rozgar_daily;
use App\Models\zong_rozgar_monthly;

use App\Models\zong_sciencelab_daily;
use App\Models\zong_sciencelab_monthly;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\FilterConfig;
use App\Models\Shared;


class ZongController extends Controller
{
    //

    public function stats($sid, $level, $it)
    {
        $op = Definition::ZONG_OP;
        $col = Shared::getColumns($op, $sid, $level, $it);

        $tblName = Definition::getCurrentModel($op, $sid, $level, $it);

        $query = DB::getDoctrineConnection()->createQueryBuilder();
        $query
            ->select([
                '*',
                //'platformbase',
                // 'minutes'
            ])
            ->from($tblName);
        $cfg = (new GridConfig())
            ->setDataProvider(new DbalDataProvider($query))
            ->setPageSize(8)
            ->setColumns($col);
        $grid = new Grid($cfg);
        /* $grid = Grids::make($cfg);*/
        return view('zong.stats', compact('grid'));
    }

    public function bizzDaily()
    {
        $bizzZongDaily = zong_bizstore_daily::orderBy('date', 'DESC')->paginate(8);
        return view('zong.bizzstore.daily', compact('bizzZongDaily'));
    }

    public function bizzMonthly()
    {
        $bizzZongMonthly = zong_bizstore_monthly::orderBy('date', 'DESC')->paginate(8);
        return view('zong.bizzstore.monthly', compact('bizzZongMonthly'));
    }

    public function cpDaily()
    {
        $cpZongDaily = zong_cp_daily::orderBy('date', 'DESC')->paginate(8);
        return view('zong.cp.daily', compact('cpZongDaily'));
    }

    public function cpMonthly()
    {
        $cpZongMonthly = zong_cp_monthly::orderBy('date', 'DESC')->paginate(8);
        return view('zong.cp.monthly', compact('cpZongMonthly'));
    }

    public function kcDaily()
    {
        $kcZongDaily = zong_kc_daily::orderBy('date', 'DESC')->paginate(8);
        return view('zong.kc.daily', compact('kcZongDaily'));
    }

    public function kcMonthly()
    {
        $kcZongMonthly = zong_kc_monthly::orderBy('date', 'DESC')->paginate(8);
        return view('zong.kc.monthly', compact('kcZongMonthly'));
    }

    public function langtutorDaily()
    {
        $langtutorDaily = zong_langtutor_daily::orderBy('date', 'DESC')->paginate(8);
        return view('zong.langtutor.daily', compact('langtutorDaily'));
    }

    public function langtutorMonthly()
    {
        $langtutorMonthly = zong_langtutor_monthly::orderBy('date', 'DESC')->paginate(8);
        return view('zong.langtutor.monthly', compact('langtutorMonthly'));
    }

    public function rbtDaily()
    {
        $rbtZongDaily = zong_rbt_daily::orderBy('date', 'DESC')->paginate(8);
        return view('zong.rbt.daily', compact('rbtZongDaily'));
    }

    public function rbtMonthly()
    {
        $rbtZongMonthly = zong_rbt_monthly::orderBy('date', 'DESC')->paginate(8);
        return view('zong.rbt.monthly', compact('rbtZongMonthly'));
    }

    public function rozgarDaily()
    {
        $rozgarZongDaily = zong_rozgar_daily::orderBy('date', 'DESC')->paginate(8);
        return view('zong.rozgar.daily', compact('rozgarZongDaily'));
    }

    public function rozgarMonthly()
    {
        $rozgarZongMonthly = zong_rozgar_monthly::orderBy('date', 'DESC')->paginate(8);
        return view('zong.rozgar.monthly', compact('rozgarZongMonthly'));
    }

    public function sciencelabDaily()
    {
        $sciencelabZongDaily = zong_sciencelab_daily::orderBy('date', 'DESC')->paginate(8);
        return view('zong.sciencelab.daily', compact('sciencelabZongDaily'));
    }

    public function sciencelabMonthly()
    {
        $sciencelabZongMonthly = zong_sciencelab_monthly::orderBy('date', 'DESC')->paginate(10);
        return view('zong.sciencelab.monthly', compact('sciencelabZongMonthly'));
    }

}
