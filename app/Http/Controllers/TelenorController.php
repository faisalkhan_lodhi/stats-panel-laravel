<?php

namespace App\Http\Controllers;

use App\Models\Definition;
use Illuminate\Http\Request;
use DB;
use Grids;
use App\Models\telenor_bizstore_daily;
use App\Models\telenor_bizstore_monthly;
use App\Models\telenor_foodcourt_daily;
use App\Models\telenor_foodcourt_monthly;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\FilterConfig;
use App\Models\Shared;


class TelenorController extends Controller
{
    public function stats($sid, $level, $it)
    {
        $op = Definition::TELENOR_OP;
        $col = Shared::getColumns($op, $sid, $level, $it);

        $tblName = Definition::getCurrentModel($op, $sid, $level, $it);
        $query = DB::getDoctrineConnection()->createQueryBuilder();
          $query
              ->select([
                  '*',
                  'obdsubs + ivrsubs + appsubs as totalsubs',

          ])
           ->from($tblName);
          $cfg = (new GridConfig())
                  ->setDataProvider(new DbalDataProvider($query))
                  ->setPageSize(8)
                  ->setColumns($col);
          $grid = new Grid($cfg);
        /* $grid = Grids::make($cfg);*/
        return view('telenor.stats', compact('grid'));
    }

//    public function daily()
//    {
//        $bizzstoreDaily = telenor_bizstore_daily::orderBy('date', 'DESC')->paginate(8);
//        return view('telenor.bizzstore.daily', compact('bizzstoreDaily'));
//    }
//
//    public function monthly()
//    {
//        $bizzstoreMonthly =  telenor_bizstore_monthly::orderBy('date', 'DESC')->paginate(8);
//        return view('telenor.bizzstore.monthly', compact('bizzstoreMonthly'));
//    }
//
//    public function foodDaily()
//    {
//       $foodcourtDaily =  telenor_foodcourt_daily::orderBy('date', 'DESC')->paginate(8);
//        return view('telenor.foodcourt.daily', compact('foodcourtDaily'));
//    }
//
//    public function foodMonthly()
//    {
//        $foodcourtMonthly =  telenor_foodcourt_monthly::orderBy('date', 'DESC')->paginate(8);
//        return view('telenor.foodcourt.monthly', compact('foodcourtMonthly'));
//    }
}