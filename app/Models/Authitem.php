<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Authitem
 * 
 * @property string $name
 * @property int $type
 * @property string $description
 * @property string $bizrule
 * @property string $data
 *
 * @package App\Models
 */
class Authitem extends Eloquent
{
	protected $table = 'authitem';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'type' => 'int'
	];

	protected $fillable = [
		'type',
		'description',
		'bizrule',
		'data'
	];
}
