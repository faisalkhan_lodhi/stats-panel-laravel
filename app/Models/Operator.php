<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Operator
 * 
 * @property int $id
 * @property string $name
 *
 * @package App\Models
 */
class Operator extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name'
	];
}
