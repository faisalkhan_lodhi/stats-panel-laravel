<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Shortcode
 * 
 * @property int $id
 * @property string $name
 * @property string $code
 *
 * @package App\Models
 */
class Shortcode extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'code'
	];
}
