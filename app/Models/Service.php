<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Service
 * 
 * @property int $serviceID
 * @property string $title
 *
 * @package App\Models
 */
class Service extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'title'
	];
}
