<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\FilterConfig;
use Grids;

class Definition extends Model
{
   const TELENOR_OP = 'telenor';
   const MOBILINK_OP = 'mobilink';
   const WARID_OP = 'warid';
   const ZONG_OP = 'zong';
   const UFONE_OP = 'ufone';

   const BIZSTORE_SER = 'Biztore';
   const VOILER_SER = 'voiler';
   const FOODCOURT_SER = 'foodcourt';
   const ROZGAR_SER = 'rozgar';
   const KC_SER = 'kc';
   const ISLAM_SER = 'islam360';
   const SCIENCE_SER = 'sciencelab';
   const RBT_SER = 'rbt';
   const URADIO_SER = 'uradio';
   const CP_SER = 'cp';
   const LANGUAGE_SER = 'languagelearning';

    public static function menuModel()
    {

           return array(
               'telenor' => array(
                   'bizstore',
                   'foodcourt',
               ),
               'mobilink' => array(
                   'bizstore',
                   'rozgar',
                   'kc',
                   'islam360',
                   'sciencelab',
               ),
               'ufone' => array(
                     'bizstore',
                     'foodcourt',
                     'uradio'
               ),
               'warid' => array(
                     'bizstore',
                     'cp',
               ),
               'zong' => array(
                       'bizstore',
                       'cp',
                       'kc',
                       'languagelearning',
                       'rozgar',
                       'sciencelab',
               ),
           );
     }

    public static function getCurrentModel($op, $sid, $level, $it)
    {
        $models = array(
            'mobilink' => array(
                'bizstore' => array(
                    'hourly' => 'MobilinkBizstoreHourly',
                    'daily' => 'mobilink_bizstore_daily',
                    'monthly' => 'mobilink_bizstore_monthly',
                ),
                'voiler' => array(
                    'hourly' => 'VoilerViewHourly',
                    'daily' => 'VoilerViewDaily',
                    'monthly' => 'VoilerViewMonthly',
                ),
                'cc' => array(
                    'hourly' => 'MobilinkCcHourly',
                    'daily' => 'mobilink_cc_daily',
                    'monthly' => 'mobilink_cc_monthly',
                ),
                'rozgar' => array(
                    'hourly' => 'MobilinkRozgarHourly',
                    'daily' => 'mobilink_rozgar_daily',
                    'monthly' => 'mobilink_rozgar_monthly',
                ),
                'kc' => array(
                    'hourly' => 'MobilinkKcHourly',
                    'daily' => 'mobilink_kc_daily',
                    'monthly' => 'mobilink_kc_monthly',
                ),
                'sciencelab' => array(
                    'hourly' => 'MobilinkSciencelabHourly',
                    'daily' => 'mobilink_sciencelab_daily',
                    'monthly' => 'mobilink_sciencelab_monthly',
                ),
                'islam360' => array(
                    'hourly' => 'MobilinkIslamHourly',
                    'daily' => 'mobilink_islam_daily',
                    'monthly' => 'mobilink_islam_monthly',
                ),
            ),
            'ufone' => array(
                'bizstore' => array(
                    'hourly' => 'UfoneBizstoreHourly',
                    'daily' => 'ufone_bizstore_daily',
                    'monthly' => 'ufone_bizstore_monthly',
                ),
                'foodcourt' => array(
                    'hourly' => 'UfoneFoodcourtHourly',
                    'daily' => 'ufone_foodcourt_daily',
                    'monthly' => 'ufone_foodcourt_monthly',
                ),
                'songsearch' => array(
                    'hourly' => 'UfoneRbtHourly',
                    'daily' => 'ufone_rbt_daily',
                    'monthly' => 'ufone_rbt_monthly',
                ),
                'voiler' => array(
                    'hourly' => 'VoilerViewHourly',
                    'daily' => 'VoilerViewDaily',
                    'monthly' => 'VoilerViewMonthly',
                ),
                'uradio' => array(
                    'hourly' => 'UfoneUradioHourly',
                    'daily' => 'ufone_uradio_daily',
                    'monthly' => 'ufone_uradio_monthly',
                ),
            ),
            'zong' => array(
                'cp' => array(
                    'hourly' => 'ZongCpHourly',
                    'daily' => 'zong_cp_daily',
                    'monthly' => 'zong_cpc-monthly',
                ),
                'bizstore' => array(
                    'hourly' => 'ZongBizstoreHourly',
                    'daily' => 'zong_bizstore_daily',
                    'monthly' => 'zong_bizstore_monthly',
                ),
                'languagelearning' => array(
                    'hourly' => 'ZongLangtutorHourly',
                    'daily' => 'zong_langtutor_daily',
                    'monthly' => 'zong_langtutor_monthly',
                ),
                'voiler' => array(
                    'hourly' => 'VoilerViewHourly',
                    'daily' => 'VoilerViewDaily',
                    'monthly' => 'VoilerViewMonthly',
                ),
                'kc' => array(
                    'hourly' => 'ZongKcHourly',
                    'daily' => 'zong_kc_daily',
                    'monthly' => 'zong_kc_monthly',
                ),
                'sciencelab' => array(
                    'hourly' => 'ZongSciencelabHourly',
                    'daily' => 'zong_sciencelab_daily',
                    'monthly' => 'zong_sciencelab_monthly',
                ),
                'rbt' => array(
                    'hourly' => 'ZongRbtHourly',
                    'daily' => 'zong_rbt_daily',
                    'monthly' => 'zong_rbt_monthly',
                ),
                'rozgar' => array(
                    'hourly' => 'ZongRozgarHourly',
                    'daily' => 'zong_rozgar_daily',
                    'monthly' => 'zong_rozgar_monthly',
                ),
            ),
            'telenor' => array(
                'bizstore' => array(
                    'hourly' => 'TelenorBizstoreHourly',
                    'daily' => 'telenor_bizstore_daily',
                    'monthly' => 'telenor_bizstore_monthly',
                ),
                'voiler' => array(
                    'hourly' => 'VoilerViewHourly',
                    'daily' => 'VoilerViewDaily',
                    'monthly' => 'VoilerViewMonthly',
                ),
                'foodcourt' => array(
                    'hourly' => 'TelenorFoodcourtHourly',
                    'daily' => 'telenor_foodcourt_daily',
                    'monthly' => 'telenor_foodcourt_monthly',
                ),
            ),
            'warid' => array(
                'bizstore' => array(
                    'hourly' => 'WaridBizstoreHourly',
                    'daily' => 'warid_bizstore_daily',
                    'monthly' => 'warid_bizstore_monthly',
                ),
                'cp' => array(
                    'hourly' => 'WaridCpHourly',
                    'daily' => 'warid_cp_daily',
                    'monthly' => 'warid_cp_monthly',
                ),
                'voiler' => array(
                    'hourly' => 'VoilerViewHourly',
                    'daily' => 'VoilerViewDaily',
                    'monthly' => 'VoilerViewMonthly',
                ),
            ),
            'du' => array(
                'bizstore' => array(
                    'hourly' => 'DuBizstoreHourly',
                    'daily' => 'DuBizstoreDaily',
                    'monthly' => 'DuBizstoreMonthly',
                ),
            ),
            'ooredoo' => array(
                'bizstore' => array(
                    'hourly' => 'OoredooBizstoreHourly',
                    'daily' => 'OoredooBizstoreDaily',
                    'monthly' => 'OoredooBizstoreMonthly',
                ),
            ),

            'ooredoo1' => array(
                'bizstore' => array(
                    'hourly' => 'OoredooBizstoreHourly',
                    'daily' => 'OoredooBizstoreDaily',
                    'monthly' => 'OoredooBizstoreMonthly',
                ),
            ),
        );

        if (array_key_exists($op, $models)) {
            $operatorArr = $models[$op];
        }

        if (isset($operatorArr) && sizeof($operatorArr) != 0):
            $serviceArr = $operatorArr[$sid];
        endif;

        if ($it == 'daily') {
            $serviceModel = $serviceArr['daily'];
        }
        elseif($it == 'hourly'){
            $serviceModel = $serviceArr['hourly'];
        }
        else{
            $serviceModel = $serviceArr['monthly'];
        }

        return ($serviceModel) ? $serviceModel : $serviceModel = array();
    }

}
