<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Celebrity
 * 
 * @property int $id
 * @property int $version
 * @property int $code
 * @property string $name
 * @property int $sort_order
 * @property bool $is_active
 *
 * @package App\Models
 */
class Celebrity extends Eloquent
{
	protected $table = 'celebrity';
	public $timestamps = false;

	protected $casts = [
		'version' => 'int',
		'code' => 'int',
		'sort_order' => 'int',
		'is_active' => 'bool'
	];

	protected $fillable = [
		'version',
		'code',
		'name',
		'sort_order',
		'is_active'
	];
}
