<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\FilterConfig;
use Grids;

class Shared extends Model
{
    //
    public static function getColumns($op, $sid, $level, $it){

        $columnName = array(
            'telenor' => array(
                'bizstore' => array(
                    '0' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Plarform Base'),
                        (new FieldConfig('totalsubs'))->setLabel('Total Subs'),
                        (new FieldConfig('obdsubs'))->setLabel('OBD Subs'),
                        (new FieldConfig('ivrsubs'))->setLabel('IVR Subs'),
                        (new FieldConfig('appsubs'))->setLabel('App Subs'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                    '1' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('totalsubs'))->setLabel('Total Subs'),
                        (new FieldConfig('obdsubs'))->setLabel('OBD Subs'),
                        (new FieldConfig('ivrsubs'))->setLabel('IVR Subs'),
                        (new FieldConfig('appsubs'))->setLabel('APP Subs'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('navigation'))->setLabel('Navigation'),
                        (new FieldConfig('ladies'))->setLabel('Ladies'),
                        (new FieldConfig('topdeals'))->setLabel('Top Deals'),
                        (new FieldConfig('entertainment'))->setLabel('Entertainment'),
                        (new FieldConfig('buy'))->setLabel('Buy'),
                        (new FieldConfig('rent'))->setLabel('Rent'),
                        (new FieldConfig('lifestyle'))->setLabel('Lifestyle & Fashion'),
                        (new FieldConfig('shopping'))->setLabel('Shopping'),
                        (new FieldConfig('resturants'))->setLabel('Resturants'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                ),
                'voiler' => array(
                    '0' => array(),
                    '1' => array(),
                ),
                'foodcourt' => array(
                    '0' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('obdsubs'))->setLabel('OBD Subs'),
                        (new FieldConfig('ivrsubs'))->setLabel('IVR Subs'),
                        (new FieldConfig('appsubs'))->setLabel('APP Subs'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                    '1' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('obdsubs'))->setLabel('OBD Subs'),
                        (new FieldConfig('ivrsubs'))->setLabel('IVR Subs'),
                        (new FieldConfig('appsubs'))->setLabel('APP Subs'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('navigation'))->setLabel('Navigation'),
                        (new FieldConfig('resturants'))->setLabel('Resturants'),
                        (new FieldConfig('hotdeals'))->setLabel('Hot Deals'),
                        (new FieldConfig('todayspecial'))->setLabel('Today Special'),
                        (new FieldConfig('dietplan'))->setLabel('Diet Plan'),
                        (new FieldConfig('recipes'))->setLabel('Recipes'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                ),
            ),

            'mobilink' => array(
                'bizstore' => array(
                    '0' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                    '1' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('nonchargedpurging'))->setLabel('Non Charged Purging'),
                        (new FieldConfig('searches'))->setLabel('Searches'),
                        (new FieldConfig('navigation'))->setLabel('Navigation'),
                        (new FieldConfig('ladies'))->setLabel('Ladies'),
                        (new FieldConfig('topdeals'))->setLabel('Top Deals'),
                        (new FieldConfig('entertainment'))->setLabel('Entertainment'),
                        (new FieldConfig('education'))->setLabel('Education'),
                        (new FieldConfig('health'))->setLabel('Health'),
                        (new FieldConfig('shopping'))->setLabel('Shopping'),
                        (new FieldConfig('resturants'))->setLabel('Resturants'),
                        (new FieldConfig('appsubs'))->setLabel('App Subs'),
                        (new FieldConfig('appunsubs'))->setLabel('App Unsubs'),
                        (new FieldConfig('appbase'))->setLabel('App Base'),
                        (new FieldConfig('chargingmessage'))->setLabel('Charging Message'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                ),
                'voiler' => array(
                    '0' => array(),
                    '1' => array(),
                ),
                'rozgar' => array(
                    '0' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base	'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs	'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                    '1' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('newsubsrecurring'))->setLabel('New Subs Recurring'),
                        (new FieldConfig('navigation'))->setLabel('Navigation'),
                        (new FieldConfig('TopJobs'))->setLabel('Top Jobs'),
                        (new FieldConfig('Construction'))->setLabel('Construction'),
                        (new FieldConfig('Carpenter'))->setLabel('Carpenter'),
                        (new FieldConfig('Electrician'))->setLabel('Electrician'),
                        (new FieldConfig('Mechanic'))->setLabel('Mechanic'),
                        (new FieldConfig('Postpaid_Renew_Billing_Alert'))->setLabel('Postpaid Renew Billing Alert'),
                        (new FieldConfig('Prepaid_Pre_Billing_Alert'))->setLabel('Prepaid Pre Billing Alert'),
                        (new FieldConfig('Purged'))->setLabel('Purged'),
                        (new FieldConfig('Minutes'))->setLabel('Minutes'),
                        (new FieldConfig('IncompleteProfiles'))->setLabel('Incomplete Profiles'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                ),
                'kc' => array(
                    '0' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs	'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                    '1' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('appsubs'))->setLabel('App Subs'),
                        (new FieldConfig('nonchargedpurging'))->setLabel('Non Charged Purging'),
                        (new FieldConfig('searches'))->setLabel('Searches'),
                        (new FieldConfig('navigation'))->setLabel('Navigation'),
                        (new FieldConfig('AajKaSpecial'))->setLabel('Aaj Ka Special'),
                        (new FieldConfig('MeraDin'))->setLabel('Mera Din'),
                        (new FieldConfig('KisiAurKaDin'))->setLabel('Kisi Aur Ka Din'),
                        (new FieldConfig('AapKaJor'))->setLabel('Aap Ka Jor'),
                        (new FieldConfig('KisiAurKaJor'))->setLabel('Kisi Aur Ka Jor'),
                        (new FieldConfig('ListenAnhoniKahanian'))->setLabel('Listen Anhoni Kahanian'),
                        (new FieldConfig('RecordAnhoniKahanian'))->setLabel('Record Anhoni Kahanian'),
                        (new FieldConfig('KhawabTabeerQuestionSent'))->setLabel('Khawab Tabeer Question Sent'),
                        (new FieldConfig('AapKiKhoobiyan'))->setLabel('Aap Ki Khoobiyan'),
                        (new FieldConfig('DoosronKiKhoobiyan'))->setLabel('Doosron Ki Khoobiyan'),
                        (new FieldConfig('AapKiKismatKaHaal'))->setLabel('Aap Ki Kismat Ka Haal'),
                        (new FieldConfig('LiveKismetKaHaal'))->setLabel('Live Kismet Ka Haal'),
                        (new FieldConfig('IncompleteProfiles'))->setLabel('Incomplete Profiles'),
                        (new FieldConfig('ProfilesAdded'))->setLabel('Profiles Added'),
                        (new FieldConfig('charged'))->setLabel('Charged'),
                        (new FieldConfig('chargeper'))->setLabel('Chargeper'),
                        (new FieldConfig('Minutes'))->setLabel('Minutes'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                ),
                'islam360' => array(
                    '0' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                    '1' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('newsubsrecurring'))->setLabel('New subs recurring'),
                        (new FieldConfig('smssubs'))->setLabel('Smssubs'),
                        (new FieldConfig('ivrsubs'))->setLabel('Ivr Subs'),
                        (new FieldConfig('obdsubs'))->setLabel('Obd Subs'),
                        (new FieldConfig('appsubs'))->setLabel('App Subs'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('navigation'))->setLabel('Navigation'),
                        (new FieldConfig('quran'))->setLabel('Quran'),
                        (new FieldConfig('Ibadaat'))->setLabel('Ibadaat'),
                        (new FieldConfig('masnoon_duain'))->setLabel('Masnoon Duain'),
                        (new FieldConfig('Purged'))->setLabel('Purged'),
                        (new FieldConfig('Minutes'))->setLabel('Minutes'),
                        (new FieldConfig('CompleteProfiles'))->setLabel('Complete Profiles'),
                        (new FieldConfig('notbilled'))->setLabel('Not billed'),
                        (new FieldConfig('champbilled'))->setLabel('Champ billed'),
                        (new FieldConfig('nonchampbilled'))->setLabel('Non champ billed'),
                        (new FieldConfig('postpaidbilled'))->setLabel('Postpaid billed'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                ),
                'sciencelab' => array(
                    '0' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                    '1' => array(
                        (new FieldConfig('date'))->setLabel('Date'),
                        (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                        (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                        (new FieldConfig('newsubsrecurring'))->setLabel('New Subs Recurring'),
                        (new FieldConfig('nonchargedpurging'))->setLabel('Non Charged Purging'),
                        (new FieldConfig('navigation'))->setLabel('Navigation'),
                        (new FieldConfig('ConceptoftheDay'))->setLabel('Concept of the Day'),
                        (new FieldConfig('MockExams'))->setLabel('Mock Exams'),
                        (new FieldConfig('SchoolCollegeSearch'))->setLabel('School College Search'),
                        (new FieldConfig('AskMe'))->setLabel('Ask Me'),
                        (new FieldConfig('PracticalTips'))->setLabel('Practical Tips'),
                        (new FieldConfig('SubjectCounseling'))->setLabel('Subject Counseling'),
                        (new FieldConfig('PaperPridiction'))->setLabel('Paper Pridiction'),
                        (new FieldConfig('GeneralInformation'))->setLabel('General Information'),
                        (new FieldConfig('SetYourProfile'))->setLabel('Set Your Profile'),
                        (new FieldConfig('charged'))->setLabel('Charged'),
                        (new FieldConfig('Minutes'))->setLabel('Minutes'),
                        (new FieldConfig('billed'))->setLabel('Billed'),
                    ),
                ),
            ),

             'warid' => array(
                 'bizstore' => array(
                     '0' => array(
                         (new FieldConfig('date'))->setLabel('Date'),
                         (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                         (new FieldConfig('totatlsubs'))->setLabel('Total Subs'),
                         (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                         (new FieldConfig('billed'))->setLabel('Billed'),
                     ),
                     '1' => array(
                         (new FieldConfig('date'))->setLabel('Date'),
                         (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                         (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                         (new FieldConfig('nonchargedpurging'))->setLabel('Non Charged Purging'),
                         (new FieldConfig('searches'))->setLabel('Searches'),
                         (new FieldConfig('navigation'))->setLabel('Navigation'),
                         (new FieldConfig('ladies'))->setLabel('Ladies'),
                         (new FieldConfig('topdeals'))->setLabel('Top Deals'),
                         (new FieldConfig('entertainment'))->setLabel('Entertainment'),
                         (new FieldConfig('education'))->setLabel('Education'),
                         (new FieldConfig('health'))->setLabel('Health'),
                         (new FieldConfig('shopping'))->setLabel('Shopping'),
                         (new FieldConfig('resturants'))->setLabel('Resturants'),
                         (new FieldConfig('billed'))->setLabel('Billed'),
                     ),
                 ),

                 'cp' => array(
                     '0' => array(
                         (new FieldConfig('date'))->setLabel('Date'),
                         (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                         (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                         (new FieldConfig('billed'))->setLabel('Billed'),
                     ),
                     '1' => array(
                         (new FieldConfig('date'))->setLabel('Date'),
                         (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                         (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                         (new FieldConfig('emailsubs'))->setLabel('Email Subs'),
                         (new FieldConfig('careertipsub'))->setLabel('Career Tip Sub'),
                         (new FieldConfig('jobalertsub'))->setLabel('Job Alert Sub'),
                         (new FieldConfig('careeradvicesub'))->setLabel('Career Advice Sub'),
                         (new FieldConfig('careertipunsub'))->setLabel('Career Tip Un-Sub'),
                         (new FieldConfig('jobalertunsub'))->setLabel('Job Alert Un-Sub'),
                         (new FieldConfig('careeradviceunsub'))->setLabel('Career Advice Un-Sub'),
                         (new FieldConfig('purged'))->setLabel('Purged'),
                         (new FieldConfig('notificationsent'))->setLabel('Notification Sent'),
                         (new FieldConfig('navigation_4545'))->setLabel('Navigation 4545'),
                         (new FieldConfig('navigation_45451'))->setLabel('Navigation 45451'),
                         (new FieldConfig('notbilled'))->setLabel('Notbilled'),
                         (new FieldConfig('billed'))->setLabel('Billed'),
                     ),
                 ),
             ),

              'ufone' => array(
                  'bizstore' => array(
                      '0' => array(
                          (new FieldConfig('date'))->setLabel('Date'),
                          (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                          (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                          (new FieldConfig('billed'))->setLabel('Billed'),
                      ),
                      '1' => array(
                          (new FieldConfig('date'))->setLabel('Date'),
                          (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                          (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                          (new FieldConfig('nonchargedpurging'))->setLabel('Non Charged Purging'),
                          (new FieldConfig('searches'))->setLabel('Searches'),
                          (new FieldConfig('navigation'))->setLabel('Navigation'),
                          (new FieldConfig('ladies'))->setLabel('Ladies'),
                          (new FieldConfig('topdeals'))->setLabel('Top Deals'),
                          (new FieldConfig('entertainment'))->setLabel('Entertainment'),
                          (new FieldConfig('education'))->setLabel('Education'),
                          (new FieldConfig('health'))->setLabel('Health'),
                          (new FieldConfig('shopping'))->setLabel('Shopping'),
                          (new FieldConfig('resturants'))->setLabel('Resturants'),
                          (new FieldConfig('billed'))->setLabel('Billed'),
                      ),
                  ),
                  'voiler' => array(
                      '0' => array(),
                      '1' => array(),
                  ),
                  'foodcourt' => array(
                      '0' => array(
                          (new FieldConfig('date'))->setLabel('Date'),
                          (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                          (new FieldConfig('obdsubs'))->setLabel('OBD Subs'),
                          (new FieldConfig('ivrsubs'))->setLabel('IVR Subs'),
                          (new FieldConfig('appsubs'))->setLabel('App Subs'),
                          (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                          (new FieldConfig('charged'))->setLabel('Charged'),
                      ),
                      '1' => array(
                          (new FieldConfig('date'))->setLabel('Date'),
                          (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                          (new FieldConfig('obdsubs'))->setLabel('OBD Subs'),
                          (new FieldConfig('ivrsubs'))->setLabel('IVR Subs'),
                          (new FieldConfig('appsubs'))->setLabel('APP Subs'),
                          (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                          (new FieldConfig('navigation'))->setLabel('Navigation'),
                          (new FieldConfig('resturants'))->setLabel('Resturants'),
                          (new FieldConfig('hotdeals'))->setLabel('Hot Deals'),
                          (new FieldConfig('todayspecial'))->setLabel('Today Special'),
                          (new FieldConfig('dietplan'))->setLabel('Diet Plan'),
                          (new FieldConfig('recipes'))->setLabel('Recipes'),
                          (new FieldConfig('charged'))->setLabel('Charged'),
                      ),
                  ),
                  'uradio' => array(
                      '0' => array(
                          (new FieldConfig('date'))->setLabel('Date'),
                          (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                          (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                          (new FieldConfig('billed'))->setLabel('Billed'),
                      ),
                      '1' => array(
                          (new FieldConfig('date'))->setLabel('Date'),
                          (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                          (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                          (new FieldConfig('totmins'))->setLabel('Totmins'),
                          (new FieldConfig('punjabi'))->setLabel('Punjabi'),
                          (new FieldConfig('sindhi'))->setLabel('Sindhi'),
                          (new FieldConfig('pashto'))->setLabel('Pashto'),
                          (new FieldConfig('balochi'))->setLabel('Balochi'),
                          (new FieldConfig('saraiki'))->setLabel('Saraiki'),
                          (new FieldConfig('chargeable_subs'))->setLabel('Chargeable Subs'),
                          (new FieldConfig('billed'))->setLabel('Billed'),
                      ),
                  ),

              ),

               'zong' => array(
                   'bizstore' => array(
                       '0' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                           (new FieldConfig('billed'))->setLabel('Billed'),
                       ),
                       '1' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                           (new FieldConfig('nonchargedpurging'))->setLabel('Non Charged Purging'),
                           (new FieldConfig('searches'))->setLabel('Searches'),
                           (new FieldConfig('navigation'))->setLabel('Navigation'),
                           (new FieldConfig('ladies'))->setLabel('Ladies'),
                           (new FieldConfig('topdeals'))->setLabel('Top Deals'),
                           (new FieldConfig('entertainment'))->setLabel('Entertainment'),
                           (new FieldConfig('education'))->setLabel('Education'),
                           (new FieldConfig('health'))->setLabel('Health'),
                           (new FieldConfig('shopping'))->setLabel('Shopping'),
                           (new FieldConfig('resturants'))->setLabel('Resturants'),
                           (new FieldConfig('billed'))->setLabel('Billed'),
                       ),
                   ),
                   'voiler' => array(
                       '0'=> array(),
                       '1' => array(),
                   ),
                   'cp' => array(
                       '0' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('unsubs'))->setLabel('Un-Subscribers'),
                           (new FieldConfig('billed'))->setLabel('Billed'),
                       ),
                       '1' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('unsubs'))->setLabel('Un-Subscribers'),
                           (new FieldConfig('notificationsent'))->setLabel('Notification Sent'),
                           (new FieldConfig('navigation_4040'))->setLabel('Navigation 4040'),
                           (new FieldConfig('verificationemails'))->setLabel('Verification Email\'s'),
                           (new FieldConfig('billed'))->setLabel('Billed'),
                       ),
                   ),
                   'kc' => array(
                       '0' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                           (new FieldConfig('charged'))->setLabel('Charged'),
                       ),
                       '1' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('smssubs'))->setLabel('SMS Subs'),
                           (new FieldConfig('ivrsubs'))->setLabel('IVR Subs'),
                           (new FieldConfig('obdsubs'))->setLabel('OBD Subs'),
                           (new FieldConfig('appsubs'))->setLabel('App Subs'),
                           (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                           (new FieldConfig('nonchargedpurging'))->setLabel('Non charged Purging'),
                           (new FieldConfig('navigation'))->setLabel('Navigation'),
                           (new FieldConfig('AajKaSpecial'))->setLabel('Aaj Ka Special'),
                           (new FieldConfig('ListenAnhoniKahanian'))->setLabel('Anhoni Kahanian'),
                           (new FieldConfig('KhawabTabeerQuestionSent'))->setLabel('Khawab Tabeer'),
                           (new FieldConfig('AapKiKhoobiyan'))->setLabel('Khoobian Khamiyan'),
                           (new FieldConfig('AapKiKismatKaHaal'))->setLabel('Aap Ki Kismat Ka Haal'),
                           (new FieldConfig('IncompleteProfiles'))->setLabel('Incomplete Profiles'),
                           (new FieldConfig('minutes'))->setLabel('Minutes'),
                           (new FieldConfig('ProfilesAdded'))->setLabel('Profiles Added'),
                           (new FieldConfig('LiveKismetKaHaal'))->setLabel('Live Kismet Ka Haal'),
                           (new FieldConfig('charged'))->setLabel('Charged'),
                       ),
                   ),
                   'languagelearning' => array(
                       '0' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                           (new FieldConfig('billed'))->setLabel('Billed'),
                       ),
                       '1' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                           (new FieldConfig('nonchargedpurging'))->setLabel('Non Charged Purging'),
                           (new FieldConfig('navigation'))->setLabel('Navigation'),
                           (new FieldConfig('tolearnlanguage'))->setLabel('To Learn Language'),
                           (new FieldConfig('dailycoaching'))->setLabel('Daily Coaching'),
                           (new FieldConfig('alllectures'))->setLabel('All Lectures'),
                           (new FieldConfig('skilltest'))->setLabel('Skill Test'),
                           (new FieldConfig('Minutes'))->setLabel('Minutes'),
                           (new FieldConfig('billed'))->setLabel('Billed'),
                       ),
                   ),

                   'rbt' => array(
                       '0' => array(),
                       '1' => array(),
                   ),

                   'rozgar' => array(
                       '0' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                           (new FieldConfig('charged'))->setLabel('Charged'),
                       ),
                       '1' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('smssubs'))->setLabel('SMS Subs'),
                           (new FieldConfig('ivrsubs'))->setLabel('IVR Subs'),
                           (new FieldConfig('obdsubs'))->setLabel('OBD Subs'),
                           (new FieldConfig('newsubsrecurring'))->setLabel('New Subs Recurring'),
                           (new FieldConfig('unsubs'))->setLabel('Un-subs'),
                           (new FieldConfig('navigation'))->setLabel('Navigation'),
                           (new FieldConfig('TopJobs'))->setLabel('Top Jobs'),
                           (new FieldConfig('Construction'))->setLabel('Construction'),
                           (new FieldConfig('Carpenter'))->setLabel('Carpenter'),
                           (new FieldConfig('Electrician'))->setLabel('Electrician'),
                           (new FieldConfig('Mechanic'))->setLabel('Mechanic'),
                           (new FieldConfig('Minutes'))->setLabel('Minutes'),
                           (new FieldConfig('IncompleteProfiles'))->setLabel('Incomplete Profiles'),
                           (new FieldConfig('charged'))->setLabel('Charged'),
                       ),
                   ),

                   'sciencelab' => array(
                       '0' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                           (new FieldConfig('charged'))->setLabel('Charged'),
                       ),
                       '1' => array(
                           (new FieldConfig('date'))->setLabel('Date'),
                           (new FieldConfig('platformbase'))->setLabel('Platform Base'),
                           (new FieldConfig('smssubs'))->setLabel('SMS Subs'),
                           (new FieldConfig('ivrsubs'))->setLabel('IVR Subs'),
                           (new FieldConfig('obdsubs'))->setLabel('OBD Subs'),
                           (new FieldConfig('newsubsrecurring'))->setLabel('New Subs Recurring'),
                           (new FieldConfig('unsubs'))->setLabel('Un-Subs'),
                           (new FieldConfig('nonchargedpurging'))->setLabel('Non Charged Purging'),
                           (new FieldConfig('navigation'))->setLabel('Navigation'),
                           (new FieldConfig('ConceptoftheDay'))->setLabel('Concept of the Day'),
                           (new FieldConfig('MockExams'))->setLabel('Mock Exams'),
                           (new FieldConfig('SchoolCollegeSearch'))->setLabel('School College Search'),
                           (new FieldConfig('AskMe'))->setLabel('Ask Me'),
                           (new FieldConfig('PracticalTips'))->setLabel('Practical Tips'),
                           (new FieldConfig('SubjectCounseling'))->setLabel('Subject Counseling'),
                           (new FieldConfig('PaperPridiction'))->setLabel('Paper Pridiction'),
                           (new FieldConfig('GeneralInformation'))->setLabel('General Information'),
                           (new FieldConfig('SetYourProfile'))->setLabel('Set Your Profile'),
                           (new FieldConfig('Minutes'))->setLabel('Minutes'),
                           (new FieldConfig('charged'))->setLabel('Charged'),
                       ),
                   ),
               ),
        );


        if (array_key_exists($op, $columnName)) {
            $operatorArr = $columnName[$op];
        }

        if (isset($operatorArr) && sizeof($operatorArr) != 0):
            $serviceColumn = $operatorArr[$sid];
        endif;
        if ($level == 1)
            $serviceColumnRet = $serviceColumn[1];
        else
            $serviceColumnRet = $serviceColumn[0];

        return ($serviceColumnRet) ? $serviceColumnRet : $serviceColumnRet = array();
    }
}
