<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PromotionalStat
 * 
 * @property int $interval_id
 * @property int $operator_id
 * @property int $yd_subscriptions
 * @property int $td_subscriptions
 * @property int $short_code
 * @property string $service_name
 * @property int $sms_subs
 * @property int $obd_subs
 * @property int $ivr_subs
 * @property int $app_subs
 * @property int $interval_type
 * @property int $yd_charged
 * @property int $td_charged
 * @property float $mtd
 * @property float $chrg_per
 *
 * @package App\Models
 */
class PromotionalStat extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'interval_id' => 'int',
		'operator_id' => 'int',
		'yd_subscriptions' => 'int',
		'td_subscriptions' => 'int',
		'short_code' => 'int',
		'sms_subs' => 'int',
		'obd_subs' => 'int',
		'ivr_subs' => 'int',
		'app_subs' => 'int',
		'interval_type' => 'int',
		'yd_charged' => 'int',
		'td_charged' => 'int',
		'mtd' => 'float',
		'chrg_per' => 'float'
	];

	protected $fillable = [
		'interval_id',
		'operator_id',
		'yd_subscriptions',
		'td_subscriptions',
		'short_code',
		'service_name',
		'sms_subs',
		'obd_subs',
		'ivr_subs',
		'app_subs',
		'interval_type',
		'yd_charged',
		'td_charged',
		'mtd',
		'chrg_per'
	];
}
