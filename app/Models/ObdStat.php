<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ObdStat
 * 
 * @property int $interval_id
 * @property bool $interval_type
 * @property string $service
 * @property bool $operator_id
 * @property string $prompt
 * @property int $dialed
 * @property int $answered
 * @property int $new_subs
 * @property float $acd
 * @property float $answer_percentage
 * @property float $conv_rate
 * @property float $mtd_answer_per
 * @property float $mtd_acd_per
 * @property float $mtd_conv_per
 * @property int $duration
 *
 * @package App\Models
 */
class ObdStat extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'interval_id' => 'int',
		'interval_type' => 'bool',
		'operator_id' => 'bool',
		'dialed' => 'int',
		'answered' => 'int',
		'new_subs' => 'int',
		'acd' => 'float',
		'answer_percentage' => 'float',
		'conv_rate' => 'float',
		'mtd_answer_per' => 'float',
		'mtd_acd_per' => 'float',
		'mtd_conv_per' => 'float',
		'duration' => 'int'
	];

	protected $fillable = [
		'interval_type',
		'prompt',
		'dialed',
		'answered',
		'new_subs',
		'acd',
		'answer_percentage',
		'conv_rate',
		'mtd_answer_per',
		'mtd_acd_per',
		'mtd_conv_per',
		'duration'
	];
}
