<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Authassignment
 * 
 * @property string $itemname
 * @property string $userid
 * @property string $bizrule
 * @property string $data
 *
 * @package App\Models
 */
class Authassignment extends Eloquent
{
	protected $table = 'authassignment';
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'bizrule',
		'data'
	];
}
