<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Authitemchild
 * 
 * @property string $parent
 * @property string $child
 *
 * @package App\Models
 */
class Authitemchild extends Eloquent
{
	protected $table = 'authitemchild';
	public $incrementing = false;
	public $timestamps = false;
}
