<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class VoilerStat
 * 
 * @property int $interval_id
 * @property int $code
 * @property int $code_count
 * @property int $interval_type
 * @property bool $operator_id
 *
 * @package App\Models
 */
class VoilerStat extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'interval_id' => 'int',
		'code' => 'int',
		'code_count' => 'int',
		'interval_type' => 'int',
		'operator_id' => 'bool'
	];

	protected $fillable = [
		'code_count',
		'interval_type'
	];
}
