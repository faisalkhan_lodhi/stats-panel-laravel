<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TelenorfcSmsCounter
 * 
 * @property int $interval_id
 * @property int $sms_code
 * @property int $sms_count
 * @property int $sms_type
 *
 * @package App\Models
 */
class TelenorfcSmsCounter extends Eloquent
{
	protected $table = 'telenorfc_sms_counter';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'interval_id' => 'int',
		'sms_code' => 'int',
		'sms_count' => 'int',
		'sms_type' => 'int'
	];

	protected $fillable = [
		'sms_count',
		'sms_type'
	];
}
