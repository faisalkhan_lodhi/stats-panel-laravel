<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 29 May 2018 07:45:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Right
 * 
 * @property string $itemname
 * @property int $type
 * @property int $weight
 *
 * @package App\Models
 */
class Right extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'type' => 'int',
		'weight' => 'int'
	];

	protected $fillable = [
		'type',
		'weight'
	];
}
