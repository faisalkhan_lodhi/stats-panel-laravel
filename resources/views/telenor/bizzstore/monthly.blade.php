@extends('layouts.app')

@section('content')



    <div class="row">
        <div class="col-md-12">

            <div>
                <a href="{{ url('telenor/bizzstore/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('telenor/bizzstore/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">
                <div class="row">
                    <br>
                    <h3 align="center">Telenor Bizzstore Monthly</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Platformbase</th>
                            <th scope="col">Minutes</th>
                            <th scope="col">Newsubs</th>
                            <th scope="col">obdsubs</th>
                            <th scope="col">ivrsubs</th>
                            <th scope="col">appsubs</th>
                            <th scope="col">unsubs</th>
                            <th scope="col">NonChargedPurging</th>
                            <th scope="col">Searches</th>
                            <th scope="col">Navigation</th>
                            <th scope="col">Ladies</th>
                            <th scope="col">Buy</th>
                        </tr>
                        </thead>


                        <tbody>

                        @foreach($bizzstoreMonthly as $bizzstore)

                            <tr>
                                <td>{{ $bizzstore->date}}</td>
                                <td>{{ $bizzstore->platformbase}}</td>
                                <td>{{ $bizzstore->minutes}}</td>
                                <td>{{ $bizzstore->newsubs}}</td>
                                <td>{{ $bizzstore->obdsubs}}</td>
                                <td>{{ $bizzstore->ivrsubs}}</td>
                                <td>{{ $bizzstore->appsubs}}</td>
                                <td>{{ $bizzstore->unsubs}}</td>
                                <td>{{ $bizzstore->nonchargedpurging}}</td>
                                <td>{{ $bizzstore->searches}}</td>
                                <td>{{ $bizzstore->navigation}}</td>
                                <td>{{ $bizzstore->ladies}}</td>
                                <td>{{ $bizzstore->buy}}</td>

                            </tr>
                        @endforeach

                        </tbody>


                    </table>

                    {{ $bizzstoreMonthly->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection