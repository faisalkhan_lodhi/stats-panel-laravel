@extends('inspinia::layouts.main')

@if (auth()->check())
@section('user-avatar', 'https://www.gravatar.com/avatar/' . md5(auth()->user()->email) . '?d=mm')
@section('user-name', auth()->user()->name)
@endif

@section('breadcrumbs')
@include('inspinia::layouts.main-panel.breadcrumbs', [
  'breadcrumbs' => [
    (object) [ 'title' => 'Home', 'url' => route('home') ]
  ]
])
@endsection

@section('sidebar-menu')

  {{--for admin--}}

  @role('admin')

  <ul class="nav metismenu" id="side-menu">

    <li>
      <a href="{{ route('home') }}"><i class="fa fa-users"></i> <span class="nav-label">User Management</span><span class="fa arrow"></span></a>
      <ul class="nav nav-second-level collapse" style="height: 20px;">
        <li>
          <a href="{{ route('users.index') }}">Users</a>
        </li>
        <li>
          <a href="{{ route('roles.index') }}">Roles</a>
        </li>
      </ul>
    </li>

      <?php
      $services = \App\Models\Definition::menuModel();
      ?>

      <li>
          <a href="#"><i class="fa fa-cubes"></i> <span class="nav-label">Telenor</span><span class="fa arrow"></span></a>
          <ul class="nav nav-second-level collapse" style="">

              @foreach ($services['telenor'] as $ser)
                  <li>

                      <a href="{{ url('telenor/stats/'.$ser.'/1/daily') }}">{{ $ser }}</a>

                  </li>
              @endforeach

          </ul>
      </li>

      <li>
          <a href="#"><i class="fa fa-globe"></i> <span class="nav-label">Jazz</span><span class="fa arrow"></span></a>
          <ul class="nav nav-second-level collapse" style="">

              @foreach ($services['mobilink'] as $ser)

                  <li>
                      <a href="{{ url('mobilink/stats/'.$ser.'/1/daily') }}"><i class=""></i>{{ $ser }}</a>
                  </li>

              @endforeach

          </ul>
      </li>

      <li>
          <a href="#"><i class="fa fa-briefcase"></i> <span class="nav-label">Ufone</span><span class="fa arrow"></span></a>
          <ul class="nav nav-second-level collapse" style="">

              @foreach ($services['ufone'] as $ser)

                  <li>
                      <a href="{{ url('ufone/stats/'.$ser.'/1/daily') }}"><i class=""></i>{{ $ser }}</a>
                  </li>

              @endforeach

          </ul>
      </li>

      <li>
          <a href="#"><i class="fa fa-fax"></i> <span class="nav-label">Warid</span><span class="fa arrow"></span></a>
          <ul class="nav nav-second-level collapse" style="">

              @foreach ($services['warid'] as $ser)

                  <li>
                      <a href="{{ url('warid/stats/'.$ser.'/1/daily') }}"><i class=""></i>{{$ser}}</a>
                  </li>
              @endforeach
          </ul>
      </li>

      <li>
          <a href="#"><i class="fa fa-comment"></i> <span class="nav-label">Zong</span><span class="fa arrow"></span></a>
          <ul class="nav nav-second-level collapse" style="">

              @foreach($services['zong'] as $ser)
                  <li>
                      <a href="{{ url('zong/stats/'.$ser.'/1/daily') }}"><i class=""></i>{{ $ser }}</a>
                  </li>
              @endforeach

          </ul>
      </li>

  </ul>

  @endrole

  {{--For telenor--}}

  @role('Telenor')

  <?php

          $services = \App\Models\Definition::menuModel();
  ?>
  <ul class="nav metismenu" id="side-menu" style="padding-left:0px;">
    <li>
      <a href="#"><i class="fa fa-cubes"></i> <span class="nav-label">Telenor</span><span class="fa arrow"></span></a>
      <ul class="nav nav-second-level collapse" style="">

          @foreach($services['telenor'] as $ser)
          <li>
          <a href="{{ url('telenor/stats/telenor/{$ser}/1/daily') }}"><i class=""></i>{{ $ser }}</a>
          </li>
          @endforeach

      </ul>
    </li>
  </ul>

 @endrole


  {{--for mobilink--}}


  @role('mobilink')

  <?php

     $services = \App\Models\Definition::menuModel();

  ?>

  <ul class="nav metismenu" id="side-menu" style="padding-left:0px;">
    <li>
      <a href="#"><i class="fa fa-globe"></i> <span class="nav-label">Jazz</span><span class="fa arrow"></span></a>
       <ul class="nav nav-second-level collapse" style="">

           @foreach ($services['mobilink'] as $ser)

               <li>
                   <a href="{{ url('mobilink/stats/mobilink/{$ser}/1/daily') }}"><i class=""></i>{{ $ser }}</a>
               </li>

           @endforeach

       </ul>
    </li>
  </ul>

  @endrole


  {{--For Zong--}}

  @role('Zong')

  <?php

   $services = \App\Models\Definition::menuModel();

  ?>

  <ul class="nav metismenu" id="side-menu" style="padding-left:0px;">
      <li>
          <a href="#"><i class="fa fa-comment"></i> <span class="nav-label">Zong</span><span class="fa arrow"></span></a>

          <ul class="nav nav-second-level collapse" style="">

              @foreach($services['zong'] as $ser)

               <li>
                  <a href="{{ url('zong/stats/zong/{$ser}/1/daily') }}"><i class=""></i>{{ $ser }}</a>
               </li>

              @endforeach

          </ul>
      </li>
  </ul>

  @endrole

  {{--For Warid--}}

  @role('Warid')

      <?php

      $services = \App\Models\Definition::menuModel();

      ?>


  <ul class="nav metismenu" id="side-menu" style="padding-left:0px;">
      <li>
          <a href="#"><i class="fa fa-fax"></i> <span class="nav-label">Warid</span><span class="fa arrow"></span></a>
          <ul class="nav nav-second-level collapse" style="">

              @foreach($services['warid'] as $ser)

              <li>
                  <a href="{{ url('warid/stats/warid/{$ser}/1/daily') }}"><i class=""></i>{{ $ser }}</a>
              </li>

              @endforeach

          </ul>
      </li>
  </ul>

  @endrole

  {{--For Ufone--}}

  @role('Ufone')
    <?php

            $services = \App\Models\Definition::menuModel();

    ?>

  <ul class="nav metismenu" id="side-menu" style="padding-left:0px;">
      <li>
          <a href="#"><i class="fa fa-briefcase"></i> <span class="nav-label">Ufone</span><span class="fa arrow"></span></a>
          <ul class="nav nav-second-level collapse" style="">

              @foreach($services['ufone'] as $ser)

              <li>
                  <a href="{{ url('ufone/stats/ufone/{$ser}/1/daily') }}"><i class=""></i>{{ $ser }}</a>
              </li>

              @endforeach

          </ul>
      </li>
  </ul>


  @endrole

  @role('Company')

  <ul class="nav metismenu" id="side-menu" style="padding-left:0px;">
    <li>
      <a href="#"><i class="fa fa-key"></i> <span class="nav-label">Change Password</span></a>
    </li>

  </ul>
  @endrole

@endsection
