@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('warid/cp/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('warid/cp/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Warid CP Daily</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                        <table class="table table-striped">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Platformbase</th>
                                <th scope="col">Newsubs</th>
                                <th scope="col">Unsubs</th>
                                <th scope="col">EmailSubs</th>
                                <th scope="col">CareerTipSub</th>
                                <th scope="col">JobAlertSub</th>
                                <th scope="col">CareerAdeviceSub</th>
                                <th scope="col">CareerTipUnsub</th>
                                <th scope="col">JobAlertUnsub</th>
                                <th scope="col">CareerAdeviceUnsub</th>
                                <th scope="col">Purged</th>
                                <th scope="col">NotificationSent</th>

                            </tr>
                            </thead>


                            <tbody>

                            @foreach($cpWaridMonthly as $cp)

                                <tr>
                                    <td>{{ $cp->date}}</td>
                                    <td>{{ $cp->platformbase}}</td>
                                    <td>{{ $cp->newsubs}}</td>
                                    <td>{{ $cp->unsubs}}</td>
                                    <td>{{ $cp->emailsubs}}</td>
                                    <td>{{ $cp->careertipsub}}</td>
                                    <td>{{ $cp->jobalertsub}}</td>
                                    <td>{{ $cp->careeradvicesub}}</td>
                                    <td>{{ $cp->careertipunsub}}</td>
                                    <td>{{ $cp->jobalertunsub}}</td>
                                    <td>{{ $cp->careeradviceunsub}}</td>
                                    <td>{{ $cp->purged}}</td>
                                    <td>{{ $cp->notificationsent}}</td>
                                </tr>
                            @endforeach

                            </tbody>


                        </table>

                        {{ $cpWaridMonthly->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection