@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div>
                <a href="{{ url('zong/rozgar/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('zong/rozgar/monthly') }}" class="btn btn-success">Monthly</a>
            </div>
            <div class="ibox float-e-margins">
                <div class="row">
                    <br>
                    <h3 align="center">Zong Rozgar Monthly</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Platformbase</th>
                            <th scope="col">Newsubs</th>
                            <th scope="col">Smssubs</th>
                            <th scope="col">Ivrsubs</th>
                            <th scope="col">Obdsubs</th>
                            <th scope="col">Newsubsrecuring</th>
                            <th scope="col">Unsubs</th>
                            <th scope="col">Navigation</th>
                            <th scope="col">TopJobs</th>
                            <th scope="col">Construction</th>
                            <th scope="col">Carpenter</th>
                            <th scope="col">Electrician</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rozgarZongMonthly as $rozgar)
                            <tr>
                                <td>{{ $rozgar->date}}</td>
                                <td>{{ $rozgar->platformbase}}</td>
                                <td>{{ $rozgar->newsubs}}</td>
                                <td>{{ $rozgar->smssubs}}</td>
                                <td>{{ $rozgar->ivrsubs}}</td>
                                <td>{{ $rozgar->obdsubs}}</td>
                                <td>{{ $rozgar->newsubsrecurring}}</td>
                                <td>{{ $rozgar->unsubs}}</td>
                                <td>{{ $rozgar->navigation}}</td>
                                <td>{{ $rozgar->TopJobs}}</td>
                                <td>{{ $rozgar->Construction}}</td>
                                <td>{{ $rozgar->Carpenter}}</td>
                                <td>{{ $rozgar->Electrician}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $rozgarZongMonthly->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection