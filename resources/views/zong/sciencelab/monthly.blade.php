@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('zong/sciencelab/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('zong/sciencelab/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Zong Science Lab Monthly</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Platformbase</th>
                            <th scope="col">Newsubs</th>
                            <th scope="col">Smssubs</th>
                            <th scope="col">Ivrsubs</th>
                            <th scope="col">Obdsubs</th>
                            <th scope="col">Newsubsrecurring</th>
                            <th scope="col">Unsubs</th>
                            <th scope="col">Nonchargedpurging</th>
                            <th scope="col">Navigation</th>
                            <th scope="col">ConceptoftheDay</th>
                            <th scope="col">‏MockExams</th>
                        </tr>
                        </thead>


                        <tbody>

                        @foreach($sciencelabZongMonthly as $sciencelab)
                            <tr>
                                <td>{{ $sciencelab->date}}</td>
                                <td>{{ $sciencelab->platformbase}}</td>
                                <td>{{ $sciencelab->newsubs}}</td>
                                <td>{{ $sciencelab->smssubs}}</td>
                                <td>{{ $sciencelab->ivrsubs}}</td>
                                <td>{{ $sciencelab->obdsubs}}</td>
                                <td>{{ $sciencelab->newsubsrecurring}}</td>
                                <td>{{ $sciencelab->unsubs}}</td>
                                <td>{{ $sciencelab->nonchargedpurging}}</td>
                                <td>{{ $sciencelab->navigation}}</td>
                                <td>{{ $sciencelab->ConceptoftheDay}}</td>
                                <td>{{ $sciencelab->MockExams}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $sciencelabZongMonthly->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection