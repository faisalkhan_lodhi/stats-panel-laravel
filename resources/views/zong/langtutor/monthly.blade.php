@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('zong/langtutor/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('zong/langtutor/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Zong Langtutor Monthly</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Platformbase</th>
                            <th scope="col">Newsubs</th>
                            <th scope="col">Unsubs</th>
                            <th scope="col">Nonchargedpurging</th>
                            <th scope="col">Searches</th>
                            <th scope="col">Navigation</th>
                            <th scope="col">ToLearnLanguage/th>
                            <th scope="col">DailyCoaching</th>
                            <th scope="col">AllLectures</th>
                            <th scope="col">SkillTest</th>
                            <th scope="col">Charged</th>
                        </tr>
                        </thead>


                        <tbody>

                        @foreach($langtutorMonthly as $lang)
                            <tr>
                                <td>{{ $lang->date}}</td>
                                <td>{{ $lang->platformbase}}</td>
                                <td>{{ $lang->newsubs}}</td>
                                <td>{{ $lang->unsubs}}</td>
                                <td>{{ $lang->nonchargedpurging}}</td>
                                <td>{{ $lang->searches}}</td>
                                <td>{{ $lang->navigation}}</td>
                                <td>{{ $lang->tolearnlanguage}}</td>
                                <td>{{ $lang->dailycoaching}}</td>
                                <td>{{ $lang->alllectures}}</td>
                                <td>{{ $lang->skilltest}}</td>
                                <td>{{ $lang->charged}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $langtutorMonthly->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection