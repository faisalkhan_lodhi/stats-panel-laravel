@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('zong/cp/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('zong/cp/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Zong Career Portal Monthly</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Platformbase</th>
                            <th scope="col">Newsubs</th>
                            <th scope="col">Unsubs</th>
                            <th scope="col">Notification Sent</th>
                            <th scope="col">navigation_4040</th>
                            <th scope="col">Verification Email</th>
                            <th scope="col">Billed</th>
                            <th scope="col">Notbilled</th>

                        </tr>
                        </thead>


                        <tbody>

                        @foreach($cpZongMonthly as $cp)

                            <tr>
                                <td>{{ $cp->date}}</td>
                                <td>{{ $cp->platformbase}}</td>
                                <td>{{ $cp->newsubs}}</td>
                                <td>{{ $cp->unsubs}}</td>
                                <td>{{ $cp->notificationsent}}</td>
                                <td>{{ $cp->navigation_4040}}</td>
                                <td>{{ $cp->verificationemails}}</td>
                                <td>{{ $cp->billed}}</td>
                                <td>{{ $cp->notbilled}}</td>
                            </tr>
                        @endforeach

                        </tbody>


                    </table>

                    {{ $cpZongMonthly->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection