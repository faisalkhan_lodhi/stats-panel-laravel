@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('zong/rbt/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('zong/rbt/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Zong RBT Monthly</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">a990</th>
                            <th scope="col">a2822</th>
                            <th scope="col">a7666</th>
                            <th scope="col">a2823</th>
                            <th scope="col">a9901</th>
                            <th scope="col">a9902</th>
                            <th scope="col">a9903</th>
                            <th scope="col">a9904</th>
                            <th scope="col">a9905</th>
                            <th scope="col">a9906</th>
                            <th scope="col">a9907</th>
                            <th scope="col">a9908</th>
                            <th scope="col">a9909</th>
                            <th scope="col">a9910</th>

                        </tr>
                        </thead>


                        <tbody>

                        @foreach($rbtZongMonthly as $rbt)
                            <tr>
                                <td>{{ $rbt->date}}</td>
                                <td>{{ $rbt->a990}}</td>
                                <td>{{ $rbt->a2822}}</td>
                                <td>{{ $rbt->a7666}}</td>
                                <td>{{ $rbt->a2823}}</td>
                                <td>{{ $rbt->a9901}}</td>
                                <td>{{ $rbt->a9902}}</td>
                                <td>{{ $rbt->a9903}}</td>
                                <td>{{ $rbt->a9904}}</td>
                                <td>{{ $rbt->a9905}}</td>
                                <td>{{ $rbt->a9906}}</td>
                                <td>{{ $rbt->a9907}}</td>
                                <td>{{ $rbt->a9908}}</td>
                                <td>{{ $rbt->a9909}}</td>
                                <td>{{ $rbt->a9910}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $rbtZongMonthly->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection