@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('zong/bizzstore/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('zong/bizzstore/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Zong Bizzstore Daily</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Platformbase</th>
                            <th scope="col">Newsubs</th>
                            <th scope="col">unsubs</th>
                            <th scope="col">NonChargedPurging</th>
                            <th scope="col">Searches</th>
                            <th scope="col">Navigation</th>
                            <th scope="col">Ladies</th>
                            <th scope="col">Topdeals</th>
                            <th scope="col">Entertainment</th>
                            <th scope="col">Education</th>
                        </tr>
                        </thead>


                        <tbody>

                        @foreach($bizzZongDaily as $bizz)

                            <tr>
                                <td>{{ $bizz->date}}</td>
                                <td>{{ $bizz->platformbase}}</td>
                                <td>{{ $bizz->newsubs}}</td>
                                <td>{{ $bizz->unsubs}}</td>
                                <td>{{ $bizz->nonchargedpurging}}</td>
                                <td>{{ $bizz->searches}}</td>
                                <td>{{ $bizz->navigation}}</td>
                                <td>{{ $bizz->ladies}}</td>
                                <td>{{ $bizz->topdeals}}</td>
                                <td>{{ $bizz->entertainment}}</td>
                                <td>{{ $bizz->education}}</td>
                            </tr>
                        @endforeach

                        </tbody>


                    </table>

                    {{ $bizzZongDaily->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection