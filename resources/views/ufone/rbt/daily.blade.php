@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('ufone/rbt/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('ufone/rbt/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Ufone RBT Daily</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Platformbase</th>
                            <th scope="col">Newsubs</th>
                            <th scope="col">Retadded</th>
                            <th scope="col">Retcomplete</th>
                            <th scope="col">Retalreadypur</th>
                            <th scope="col">Apisub</th>
                            <th scope="col">a6004</th>
                            <th scope="col">a6006</th>
                            <th scope="col">a6765</th>
                            <th scope="col">a6766</th>
                        </tr>
                        </thead>


                        <tbody>

                        @foreach($rbtDaily as $rbt)

                            <tr>
                                <td>{{ $rbt->date}}</td>
                                <td>{{ $rbt->platformbase}}</td>
                                <td>{{ $rbt->newsubs}}</td>
                                <td>{{ $rbt->unsubs}}</td>
                                <td>{{ $rbt->retadded}}</td>
                                <td>{{ $rbt->retalreadypur}}</td>
                                <td>{{ $rbt->apisub}}</td>
                                <td>{{ $rbt->a6004}}</td>
                                <td>{{ $rbt->a6006}}</td>
                                <td>{{ $rbt->a6765}}</td>
                                <td>{{ $rbt->a6766}}</td>

                            </tr>
                        @endforeach

                        </tbody>


                    </table>

                    {{ $rbtDaily->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection