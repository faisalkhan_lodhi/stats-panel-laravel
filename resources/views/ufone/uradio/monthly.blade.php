@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('ufone/uradio/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('ufone/uradio/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Ufone URadio Monthly</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Platformbase</th>
                            <th scope="col">Newsubs</th>
                            <th scope="col">Unsubs</th>
                            <th scope="col">Totmins</th>
                            <th scope="col">Punjabi</th>
                            <th scope="col">Sindhi</th>
                            <th scope="col">Pashto</th>
                            <th scope="col">Balochi</th>
                            <th scope="col">Saraiki</th>
                            <th scope="col">Billed</th>
                            <th scope="col">Chargeable Stats</th>
                        </tr>
                        </thead>


                        <tbody>

                        @foreach($uradioMonthly as $uradio)

                            <tr>
                                <td>{{ $uradio->date}}</td>
                                <td>{{ $uradio->platformbase}}</td>
                                <td>{{ $uradio->newsubs}}</td>
                                <td>{{ $uradio->unsubs}}</td>
                                <td>{{ $uradio->totmins}}</td>
                                <td>{{ $uradio->punjabi}}</td>
                                <td>{{ $uradio->sindhi}}</td>
                                <td>{{ $uradio->pashto}}</td>
                                <td>{{ $uradio->balochi}}</td>
                                <td>{{ $uradio->saraiki}}</td>
                                <td>{{ $uradio->billed}}</td>
                                <td>{{ $uradio->chargeable_subs}}</td>

                            </tr>
                        @endforeach

                        </tbody>


                    </table>

                    {{ $uradioMonthly->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection