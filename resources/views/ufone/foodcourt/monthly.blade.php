@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('ufone/foodcourt/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('ufone/foodcourt/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Ufone Foodcourt Monthly</h3>
                    <br>

                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Platformbase</th>
                            <th scope="col">Newsubs</th>
                            <th scope="col">obdsubs</th>
                            <th scope="col">ivrsubs</th>
                            <th scope="col">appsubs</th>
                            <th scope="col">unsubs</th>
                            <th scope="col">NonChargedPurging</th>
                            <th scope="col">Searches</th>
                            <th scope="col">Navigation</th>
                            <th scope="col">Resturants</th>
                            <th scope="col">Hotdeals</th>
                        </tr>
                        </thead>


                        <tbody>

                        @foreach($foodcourtMonthly as $foodcourt)

                            <tr>
                                <td>{{ $foodcourt->date}}</td>
                                <td>{{ $foodcourt->platformbase}}</td>
                                <td>{{ $foodcourt->newsubs}}</td>
                                <td>{{ $foodcourt->obdsubs}}</td>
                                <td>{{ $foodcourt->ivrsubs}}</td>
                                <td>{{ $foodcourt->appsubs}}</td>
                                <td>{{ $foodcourt->unsubs}}</td>
                                <td>{{ $foodcourt->nonchargedpurging}}</td>
                                <td>{{ $foodcourt->searches}}</td>
                                <td>{{ $foodcourt->navigation}}</td>
                                <td>{{ $foodcourt->resturants}}</td>
                                <td>{{ $foodcourt->hotdeals}}</td>

                            </tr>
                        @endforeach

                        </tbody>


                    </table>

                    {{ $foodcourtMonthly->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection