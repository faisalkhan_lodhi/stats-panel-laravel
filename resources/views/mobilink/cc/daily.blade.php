@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('mobilink/cc/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('mobilink/cc/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Jazz CC Daily</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Minutes</th>
                            <th scope="col">Calls</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($CC as $C)

                            <tr>
                                <td>{{ $C->date}}</td>
                                <td>{{ $C->Minutes }}</td>
                                <td>{{ $C->Calls}}</td>
                            </tr>
                        @endforeach

                        </tbody>


                    </table>

                    {{ $CC->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection