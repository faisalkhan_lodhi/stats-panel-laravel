@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('mobilink/islam/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('mobilink/islam/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Jazz Islam Daily</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Platformbase</th>
                            <th scope="col">Newsubs</th>
                            <th scope="col">Smssubs</th>
                            <th scope="col">Ivrsubs</th>
                            <th scope="col">Obdsubs</th>
                            <th scope="col">Newsubsrecuring</th>
                            <th scope="col">appsubs</th>
                            <th scope="col">unsubs</th>
                            <th scope="col">Navigation</th>
                            <th scope="col">Quran</th>
                            <th scope="col">Ibadaat</th>
                            <th scope="col">Masnoon Duain</th>
                        </tr>
                        </thead>


                        <tbody>

                        @foreach($islamMonthly as $islam)
                            <tr>
                                <td>{{ $islam->date}}</td>
                                <td>{{ $islam->platformbase}}</td>
                                <td>{{ $islam->newsubs}}</td>
                                <td>{{ $islam->smssubs}}</td>
                                <td>{{ $islam->ivrsubs}}</td>
                                <td>{{ $islam->obdsubs}}</td>
                                <td>{{ $islam->newsubsrecurring}}</td>
                                <td>{{ $islam->appsubs}}</td>
                                <td>{{ $islam->unsubs}}</td>
                                <td>{{ $islam->navigation}}</td>
                                <td>{{ $islam->quran}}</td>
                                <td>{{ $islam->Ibadaat}}</td>
                                <td>{{ $islam->masnoon_duain}}</td>
                            </tr>
                        @endforeach

                        </tbody>


                    </table>

                    {{ $islamMonthly->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection