@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div>
                <a href="{{ url('mobilink/kc/daily') }}" class="btn btn-success">Daily</a>
                <a href="{{ url('mobilink/kc/monthly') }}" class="btn btn-success">Monthly</a>
            </div>

            <div class="ibox float-e-margins">

                <div class="row">
                    <br>
                    <h3 align="center">Jazz KC Monthly</h3>
                    <br>
                    <div class="table-responsive">
                        <a href="#" class="btn btn-info btn-sm">Basic</a>
                        <a href="#" class="btn btn-info btn-sm">Advance</a>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Platformbase</th>
                            <th scope="col">Newsubs</th>
                            <th scope="col">Smssubs</th>
                            <th scope="col">Ivrsubs</th>
                            <th scope="col">Obdsubs</th>
                            <th scope="col">Appsubs</th>
                            <th scope="col">Unsubs</th>
                            <th scope="col">Nonchargedpurging</th>
                            <th scope="col">Searches</th>
                            <th scope="col">Navigation</th>
                            <th scope="col">AajKaSpecial</th>
                            <th scope="col">MeraDin</th>
                            <th scope="col">KisiAurKaDin</th>
                        </tr>
                        </thead>


                        <tbody>

                        @foreach($kcMonthly as $kc)
                            <tr>
                                <td>{{ $kc->date}}</td>
                                <td>{{ $kc->platformbase}}</td>
                                <td>{{ $kc->newsubs}}</td>
                                <td>{{ $kc->smssubs}}</td>
                                <td>{{ $kc->ivrsubs}}</td>
                                <td>{{ $kc->obdsubs}}</td>
                                <td>{{ $kc->appsubs}}</td>
                                <td>{{ $kc->unsubs}}</td>
                                <td>{{ $kc->nonchargedpurging}}</td>
                                <td>{{ $kc->searches}}</td>
                                <td>{{ $kc->navigation}}</td>
                                <td>{{ $kc->AajKaSpecial}}</td>
                                <td>{{ $kc->MeraDin}}</td>
                                <td>{{ $kc->KisiAurKaDin}}</td>
                            </tr>
                        @endforeach

                        </tbody>


                    </table>

                    {{ $kcMonthly->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection